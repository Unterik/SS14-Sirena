reagent-name-artecalcet = artecalcet
reagent-desc-artecalcet = Silver-based drug. Used in all mind-altering drugs. In its pure form it causes poisoning.

reagent-name-arteicog = arteicog
reagent-desc-arteicog = Artecalcet-based drug. Causes a sedative effect when exposed to a reasonable person. When overdosed, it puts the target to sleep.
arteicog-effect-1 = You feel calm
arteicog-effect-2 = Your muscles are relaxed
arteicog-effect-3 = You feel serene
arteicog-effect-4 = Your mind is so relaxed that you sleep
arteicog-effect-5 = You feel your limbs are too heavy
arteicog-effect-6 = You rest comfortably without worrying about anything

reagent-name-gadoiprinim = gadoiprinim
reagent-desc-gadoiprinim = Artecalcet-based drug. Reduces pain when administered internally.
gadoiprinim-effect-1 = Your body feels less pain

reagent-name-vinikefastatin = vinikefastatin
reagent-desc-vinikefastatin = Artecalcet-based drug. A drug that activates certain parts of the brain.
vinikefastatin-effect-1 = You feel invigorated 
vinikefastatin-effect-2 = You have a lot of thoughts running through your head
vinikefastatin-effect-3 = Things don't seem so hopeless
vinikefastatin-effect-4 = You are filled with determination.
vinikefastatin-effect-5 = Your mind fills with confidence.

reagent-name-aphrodisiac = aphrodisiac
reagent-desc-aphrodisiac = Artecalcet-based drug. Stimulates sexual arousal.
aphrodisiac-effect-1 = You feel a weak sexual attraction to the nearest reasonable.
aphrodisiac-effect-2 = Your mind is filled with thoughts of sex.
aphrodisiac-effect-3 = Your legs begin to shake slightly.
aphrodisiac-effect-4 = You feel a slight discomfort from below.
aphrodisiac-effect-5 = You feel sexually attracted to your nearest sensible.
aphrodisiac-effect-6 = Your mind is filled with thoughts of sex.
aphrodisiac-effect-7 = Your legs are slightly shaky.
aphrodisiac-effect-8 = You feel discomfort from below.
aphrodisiac-effect-9 = You feel like you want reasonable people around you.
aphrodisiac-effect-10 = Your mind is filled only with thoughts of sex.
aphrodisiac-effect-11 = Your legs occasionally wobble.
aphrodisiac-effect-12 = Everything is itching from below and you want to satisfy yourself as soon as possible.
aphrodisiac-effect-13 = YOU WANT A WAY TO HAVE A WAY TO DO IT!
aphrodisiac-effect-14 = You can think of nothing but sex!
aphrodisiac-effect-15 = Your legs can't hold you!
aphrodisiac-effect-16 = Foam flows profusely from the mouth of an intelligent man.
aphrodisiac-effect-17 = The sensible one is shaking
aphrodisiac-effect-18 = Sensible's genitals are highly aroused

reagent-physical-desc-silver = silver
